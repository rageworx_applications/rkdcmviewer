# Makefile for rkDCMviewer for Linux GCC.
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
GCC = gcc
GPP = g++
AR  = ar

# TARGET settings
TARGET_PKG = rkdcmviewer
TARGET_DIR = ./bin
TARGET_OBJ = ./obj

# Installation paths
INST_PATH  = /usr/local/bin
DESK_PATH  = /usr/share/applications
IRES_PATH  = /usr/local/share/icons/hicolor/256x256/apps

# Compiler optiops
COPTS += -std=c++11
COPTS += -ffast-math -fexceptions -fopenmp -O3 -s

# FLTK configs
FCG = fltk-config --use-images
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# FLTK image toolkit
FLIMGTK = ../fl_imgtk/lib

# libtinydcm library
TDCML = ../libtinydicom/lib

# rawprocessor
RAWPC = ../librawprocessor/lib

# Sources
SRC_PATH = src
SRCS  = $(wildcard $(SRC_PATH)/*.cpp)
SRCS += $(wildcard $(SRC_PATH)/fl/*.cpp)

# Make object targets from SRCS.
OBJS  = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
OBJS += $(SRCS:$(SRC_PATH)/fl/%.cpp=$(TARGET_OBJ/%.o)

# CC FLAG
CFLAGS  = -I$(SRC_PATH) -I$(SRC_PATH)/fl -Ires
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += -I$(FLIMGTK)
CFLAGS += -I$(TDCML)
CFLAGS += -I$(RAWPC)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# LINK FLAG
#LFLAGS  = -static
LFLAGS += -L$(FLIMGTK)
LFLAGS += -L$(TDCML)
LFLAGS += -L$(RAWPC)
LFLAGS += -lfl_imgtk
LFLAGS += -ltinydicom
LFLAGS += -lrawprocessor
LFLAGS += -lpthread
LFLAGS += $(FLTKCFG_LFG)

.PHONY: prepare clean continue

all: prepare clean continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)
	@mkdir -p $(TARGET_OBJ)/fl

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o
	@rm -rf $(TARGET_OBJ)/fl/*.o

install: $(TARGET_DIR)/$(TARGET_PKG)
	@mkdir -p $(IRES_PATH)
	@cp -rf $< $(INST_PATH)
	@cp -rf res/rkcpumon.png $(IRES_PATH)
	@./mkinst.sh $(INST_PATH) $(DESK_PATH)

uninstall: $(INST_PATH)/$(TARGET_PKG)
	@rm -rf $(INST_PATH)/$(TARGET_PKG)
	@rm -rf $(IRES_PATH)/rkcpumon.png
	@rm -rf $(DESK_PATH)/rkcpumon.desktop

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS)
	@echo "Generating $@ ..."
	@$(GPP) $(OBJS) $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
