# rkDCMviewer

* An open source DICOM tag file image viewer
* Made for CD, DVD, USB automatically run host program.
* rkDCMviewer may read all files in dcms or user input directory.

## License

* MIT License, see file of "LICENSE".

## Update news ##

### version 0.2.3.26

* Applied latest version of FLTK-1.3.5-2-ts.
* Applied latest version of libtinydicom.
* Applied latest version of librawprocessor.
* Reading more DICOM files improved by libtindydicom.
* Changed theme scheme to use "flat".
* Fixed some bugs like calculating window width and level.

### version 0.2.2.24

* First commit. 

## Required libraries

* FLTK-1.3.5-2-ts,
  https://github.com/rageworx/fltk-1.3.5-2-ts
* librawprocessor,
  https://github.com/rageworx/librawprocessor
* libtinydicom,
  https://github.com/rageworx/libtinydicom
  
## Supported OS

* Windows
* Linux ( debain, all platforms )
* MacOS ( X, 11, x86.64 and arm64 )
