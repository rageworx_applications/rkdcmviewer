#ifndef __WMAIN_H__
#define __WMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Scroll.H>

#include "Fl_BorderlessWindow.H"
#include "Fl_RAWImageViewer.h"

#include <list>
#include <vector>
#include <string>

class wMain : public Fl_RAWImageViewerNotifier
{
    public:
        wMain( int argc, char** argv );

    public:
        int     Run();
        void    WidgetCB( Fl_Widget* w );
        void    MenuCB( Fl_Widget* w );
        void    MoveCB( Fl_Widget* w );

    public: /// inherits to Fl_RAWImageViewerNotifier
        void OnRightClick( int x, int y);
        void OnKeyPressed( unsigned short k, int s, int c, int a );
        void OnDrawCompleted();

    private:
        ~wMain();

    protected:
        void parseParams();
        void createComponents();
        void applyThemes();
        void arrangeChildWindows();
        void imageview( int idx );

    protected:
        int     _argc;
        char**  _argv;

    protected:
        bool         _runsatfullscreen;
        bool         _keyprocessing;
        std::string  _finddir;

    protected:
        std::vector< std::string > _dcmfiles;
        std::string     _strStudyLU;
        std::string     _strStudyRU;
        std::string     _strStudyLD;
        std::string     _strStudyRD;

    protected:
        Fl_BorderlessWindow*    mainWindow;
        Fl_Menu_Bar*            sysMenu;
        Fl_Group*               grpStatus;
        Fl_Box*                 boxStatus;
        Fl_RAWImageViewer*      comRView;
        Fl_Box*                 boxLeftUp;
        Fl_Box*                 boxRightUp;
        Fl_Box*                 boxLeftDown;
        Fl_Box*                 boxRightDown;
};

#endif /// of __WINMAIN_H__
