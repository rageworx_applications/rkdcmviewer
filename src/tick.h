#ifndef _WIN32
#ifndef __GETTICK_H__
#define __GETTICK_H__

unsigned long GetTickCount();
unsigned long GetTickCount2();

#endif /// of __GETTICK_H__
#endif /// of _WIN32
