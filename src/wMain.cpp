#ifdef _WIN32
    #include <windows.h>
#endif // of _WIN32

#include <unistd.h>
#include <dirent.h>

#include <cstdio>
#include <cstring>

#include "wMain.h"
#include <FL/fl_ask.H>
#include <FL/fl_draw.H>
#include <FL/Fl_RGB_Image.H>

#include "themes.h"
#include "resource.h"

#include "libdcm.h"
#include "rawprocessor.h"

#ifndef _WIN32
#   include "tick.h"
#endif /// of _WIN32

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "RK DICOM Viewer"
#define DEF_APP_DEFAULT_W       700
#define DEF_APP_DEFAULT_H       500

////////////////////////////////////////////////////////////////////////////////

void fl_w_cb( Fl_Widget* w, void* p );
void fl_menu_cb( Fl_Widget* w, void* p );
void fl_move_cb( Fl_Widget* w, void* p );

////////////////////////////////////////////////////////////////////////////////

Fl_Menu_Item    sysMenuLists[] =
{
    {"&Help", 0, 0, 0, FL_SUBMENU},
        {"About this program", 0,       fl_menu_cb,  (void*)30, 0 },
        {"About open sources", 0,       fl_menu_cb,  (void*)31, 0 },
        {0},
    {0},
};

////////////////////////////////////////////////////////////////////////////////

wMain::wMain( int argc, char** argv )
 : _argc( argc ),
   _argv( argv ),
   _runsatfullscreen( false ),
   _keyprocessing( false )
{
    parseParams();
    createComponents();
    arrangeChildWindows();

    // Search directory ....
    DIR*            pDir = NULL;
    struct dirent*  dirEntry;

    pDir = opendir( _finddir.c_str() );
    if ( pDir != NULL )
    {
        dirEntry = readdir( pDir );

        while( dirEntry != NULL )
        {
            char* pName = strdup( dirEntry->d_name );

            if ( pName != NULL )
            {
#ifdef DEBUG
                printf("%s\n",pName);
#endif // DEBUG
                // strupper ...
                char* beg = pName;
                while (*beg = toupper(*beg)) beg++;

                if( strstr( pName, ".DCM" ) != NULL )
                {
                    string tmpstr = pName;
                    _dcmfiles.push_back( pName );
                }

                free( pName );
            }

            dirEntry = readdir( pDir );
        }

        closedir( pDir );
    }

    if ( _dcmfiles.size() > 0 )
    {
        if ( comRView != NULL )
        {
            comRView->range( 1, _dcmfiles.size() );
        }

        imageview( 0 );
    }

#ifdef _WIN32
    HICON
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    HICON
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    SendMessage( fl_xid( mainWindow ),
                 WM_SETICON,
                 ICON_BIG,
                 (LPARAM)hIconWindowLarge );

    SendMessage( fl_xid( mainWindow ),
                 WM_SETICON,
                 ICON_SMALL,
                 (LPARAM)hIconWindowSmall );

    mainWindow->titleicon( hIconWindowSmall );

#endif // _WIN32

    if ( _runsatfullscreen == true )
    {
        mainWindow->enlarge();
    }
}

wMain::~wMain()
{

}

int wMain::Run()
{
    return Fl::run();
}

void wMain::parseParams()
{
    if ( _argc >= 1 )
    {
        for( int cnt=0; cnt<_argc; cnt++ )
        {
            string tmpp = _argv[cnt];

            if ( tmpp[0] = '-' )
            {
                if ( ( tmpp == "-fullscreen" ) || ( tmpp == "-fs" ) )
                {
                    _runsatfullscreen = true;
                }
            }
            else
            {
                _finddir = _argv[ cnt ];
            }
        }
    }

    if ( _finddir.size() == 0 )
    {
        _finddir = "./dcms";
    }
}

void wMain::createComponents()
{
    int ver_i[] = {APP_VERSION};
    static char wintitle[128] = {0};

    sprintf( wintitle, "%s, version %d.%d.%d.%d",
             DEF_APP_NAME,
             ver_i[0],
             ver_i[1],
             ver_i[2],
             ver_i[3] );

    mainWindow = new Fl_BorderlessWindow( DEF_APP_DEFAULT_W,DEF_APP_DEFAULT_H, wintitle );
    if ( mainWindow != NULL)
    {
        int cli_x = mainWindow->clientarea_x();
        int cli_y = mainWindow->clientarea_y();
        int cli_w = mainWindow->clientarea_w();
        int cli_h = mainWindow->clientarea_h();

        Fl_Group* grpMenu = new Fl_Group( cli_x, cli_y, cli_w, 20 );
        if ( grpMenu != NULL )
        {
            grpMenu->begin();
        }

            sysMenu = new Fl_Menu_Bar( cli_x, cli_y, cli_w, 20 );
            if ( sysMenu != NULL )
            {
                sysMenu->menu( sysMenuLists );
                sysMenu->callback( fl_w_cb, this );
            }

        if ( grpMenu != NULL )
        {
            grpMenu->end();
        }

        comRView = new Fl_RAWImageViewer( cli_x, cli_y + 20 , cli_w, cli_h - 40 );
        if ( comRView != NULL )
        {
            comRView->callback( fl_w_cb, this );
            comRView->notifier( this );
        }

        int box_m  = 20;
        int box_x  = cli_x + box_m;
        int box_y  = cli_y + box_m + 20;
        int boxesw = cli_w / 2 - box_m;
        int boxesh = ( cli_h - 40 ) / 2 - box_m;

        boxLeftUp = new Fl_Box( box_x, box_y, boxesw, boxesh);
        if ( boxLeftUp != NULL )
        {
            boxLeftUp->box( FL_NO_BOX );
            boxLeftUp->align( FL_ALIGN_TOP_LEFT | FL_ALIGN_INSIDE );
            boxLeftUp->labelcolor( FL_WHITE );
            boxLeftUp->labelfont( FL_COURIER );
            boxLeftUp->labelsize( 11 );
        }

        boxRightUp = new Fl_Box( box_x + boxesw, box_y, boxesw, boxesh);
        if ( boxRightUp != NULL )
        {
            boxRightUp->box( FL_NO_BOX );
            boxRightUp->align( FL_ALIGN_TOP_RIGHT | FL_ALIGN_INSIDE );
            boxRightUp->labelcolor( FL_WHITE );
            boxRightUp->labelfont( FL_COURIER );
            boxRightUp->labelsize( 11 );
        }

        boxLeftDown = new Fl_Box( box_x , box_y + boxesh, boxesw, boxesh);
        if ( boxLeftDown != NULL )
        {
            boxLeftDown->box( FL_NO_BOX );
            boxLeftDown->align( FL_ALIGN_BOTTOM_LEFT | FL_ALIGN_INSIDE );
            boxLeftDown->labelcolor( FL_WHITE );
            boxLeftDown->labelfont( FL_COURIER );
            boxLeftDown->labelsize( 11 );
        }

        boxRightDown = new Fl_Box( box_x + boxesw, box_y + boxesh, boxesw, boxesh);
        if ( boxRightDown != NULL )
        {
            boxRightDown->box( FL_NO_BOX );
            boxRightDown->align( FL_ALIGN_BOTTOM_RIGHT | FL_ALIGN_INSIDE );
            boxRightDown->labelcolor( FL_WHITE );
            boxRightDown->labelfont( FL_COURIER );
            boxRightDown->labelsize( 11 );
        }

        grpStatus = new Fl_Group( cli_x, cli_h, cli_w, 20 );
        if ( grpStatus != NULL )
        {
            grpStatus->begin();

            boxStatus = new Fl_Box( cli_x, cli_h, cli_w, 20 );
            if ( boxStatus != NULL )
            {
                boxStatus->label( "NONE." );
            }

            grpStatus->end();
        }

        mainWindow->end();
        mainWindow->callback( fl_w_cb, this );
        mainWindow->callback_onmove( fl_move_cb, this );

        int min_w_w = ( DEF_APP_DEFAULT_W  / 3 ) * 2;
        int min_w_h = ( DEF_APP_DEFAULT_H  / 3 ) * 2;

        mainWindow->size_range( min_w_w, min_w_h );

        // Set resize area ...
        if ( comRView != NULL )
        {
            mainWindow->clientarea()->resizable( comRView );
        }

        // put it center of current desktop.
        int dsk_x = 0;
        int dsk_y = 0;
        int dsk_w = 0;
        int dsk_h = 0;

        Fl::screen_work_area( dsk_x, dsk_y, dsk_w, dsk_h );

        int win_p_x = ( ( dsk_w - dsk_x ) - DEF_APP_DEFAULT_W ) / 2;
        int win_p_y = ( ( dsk_h - dsk_y ) - DEF_APP_DEFAULT_H ) / 2;

        mainWindow->position( win_p_x, win_p_y );

        mainWindow->show();
    }

    applyThemes();
}

void wMain::applyThemes()
{
    rkrawv::InitTheme();

    if ( mainWindow != NULL )
    {
        rkrawv::ApplyBWindowTheme( mainWindow );
    }

    if ( sysMenu != NULL )
    {
        rkrawv::ApplyMenuBarTheme( sysMenu );
    }

    if ( grpStatus != NULL )
    {
        rkrawv::ApplyGroupTheme( grpStatus );

        if ( boxStatus != NULL )
        {
            rkrawv::ApplyStatusBoxTheme( boxStatus );
        }
    }
}

void wMain::arrangeChildWindows()
{
}

void wMain::imageview( int idx )
{
    if ( ( idx < 0 ) || ( idx > _dcmfiles.size() - 1 ) )
    {
        return;
    }

    string dcmfpath = _finddir;
    dcmfpath += "/";
    dcmfpath += _dcmfiles[ idx ];

    if ( OpenDCMA( dcmfpath.c_str() ) == true )
    {
        int elemdcms = GetElementCount();
        if ( elemdcms > 0 )
        {
            // Find some elements ...
            ImageInformation dcmimg = {0};

            if ( ReadPixelData( &dcmimg ) == true )
            {
                int bytes = 0;

                if ( dcmimg.bpp > 32 )
                {
                    bytes = 8;
                }
                else
                if ( dcmimg.bpp > 16 )
                {
                    bytes = 4;
                }
                else
                if ( dcmimg.bpp > 8 )
                {
                    bytes = 2;
                }
                else
                {
                    bytes = 1;
                }

                RAWProcessor* rawproc = new RAWProcessor();
                if ( rawproc != NULL )
                {
                    unsigned long newbuffsz = dcmimg.width * dcmimg.height * bytes;

                    bool bLoad =
                    rawproc->LoadFromMemory( (char*)dcmimg.pixels,
                                             newbuffsz,
                                             TRANSFORM_NONE,
                                             dcmimg.width );

                    if ( bLoad == false )
                    {
                        delete rawproc;
                        return;
                    }

                    _strStudyLU.clear();
                    _strStudyRU.clear();
                    _strStudyLD.clear();
                    _strStudyRD.clear();

                    // Check study informations ...
                    // Left up box ...
                    DCMTagElement* dcmtag = FindElement( 0x00100010 ); /// Patient name
                    if ( dcmtag != NULL )
                    {
                        _strStudyLU += "Patient name: ";

                        if ( dcmtag->alloced == true )
                        {
                            _strStudyLU += (char*)dcmtag->dynamicbuffer;
                        }
                        else
                        {
                            _strStudyLU += dcmtag->staticbuffer;
                        }
                        _strStudyLU += "\n";
                    }

                    dcmtag = FindElement( 0x00100040 ); /// Patient Sex.
                    if ( dcmtag != NULL )
                    {
                        _strStudyLU += "Patient sex: ";
                        _strStudyLU += dcmtag->staticbuffer;
                        _strStudyLU += "\n";
                    }

                    dcmtag = FindElement( 0x00100030 ); /// Patient Birthday.
                    if ( dcmtag != NULL )
                    {
                        _strStudyLU += "Patient birthday: ";
                        _strStudyLU += dcmtag->staticbuffer;
                        _strStudyLU += "\n";
                    }

                    dcmtag = FindElement( 0x00080060 ); /// Modality
                    if ( dcmtag != NULL )
                    {
                        _strStudyLU += "Modality: ";
                        _strStudyLU += dcmtag->staticbuffer;
                        _strStudyLU += "\n";
                    }

                    // Right up ....
                    dcmtag = FindElement( 0x00080020 ); /// Study Date.
                    if ( dcmtag != NULL )
                    {
                        _strStudyRU += "Study Date: ";
                        _strStudyRU += dcmtag->staticbuffer;
                        _strStudyRU += "\n";
                    }

                    dcmtag = FindElement( 0x00080030 ); /// Study Time.
                    if ( dcmtag != NULL )
                    {
                        _strStudyRU += "Study Time: ";
                        _strStudyRU += dcmtag->staticbuffer;
                        _strStudyRU += "\n";
                    }

                    // Left Down ...
                    dcmtag = FindElement( 0x0020000D ); /// Study Instance UID.
                    if ( dcmtag != NULL )
                    {
                        _strStudyLD += "SI UID:\n";
                        _strStudyLD += dcmtag->staticbuffer;
                        _strStudyLD += "\n";
                    }

                    dcmtag = FindElement( 0x00280004 ); /// Photometric ...
                    if ( dcmtag != NULL )
                    {
                        _strStudyLD += dcmtag->staticbuffer;
                        _strStudyLD += "\n";
                    }

                    RAWProcessor::WeightAnalysisReport weight_report = {0};

                    unsigned w_center = 0;

                    dcmtag = FindElement( 0x00281050 ); /// window center ....
                    if ( dcmtag != NULL )
                    {
                        _strStudyRD += "W.CENTER: ";
                        _strStudyRD += dcmtag->staticbuffer;
                        _strStudyRD += "\n";

                        w_center = atoi( dcmtag->staticbuffer );
                    }

                    dcmtag = FindElement( 0x00281051 ); /// window width ...

                    if ( dcmtag != NULL )
                    {
                        _strStudyRD += "W.WIDTH: ";
                        _strStudyRD +=  dcmtag->staticbuffer;
                        _strStudyRD += "\n";

                        int w_width = atoi( dcmtag->staticbuffer );
                        if ( ( w_width > 0 ) && ( w_center == 0 ) )
                        {
                            w_center = w_width / 2;
                        }

                        int maxlvl = w_center + w_width / 2;
                        int minlvl = w_center - w_width / 2;

                        if ( maxlvl < 0 )
                        {
                            maxlvl = rawproc->MaximumLevel();
                        }

                        if ( minlvl < 0 )
                        {
                            minlvl = 0;
                        }

                        weight_report.timestamp = GetTickCount();
                        weight_report.threshold_wide_min = minlvl;
                        weight_report.threshold_wide_max = maxlvl;
                    }


                    if ( weight_report.threshold_wide_max == 0 )
                    {
                        weight_report.timestamp = GetTickCount();
                        weight_report.threshold_wide_min = rawproc->MinimumLevel();
                        weight_report.threshold_wide_max = rawproc->MaximumLevel();
                    }

                    vector<unsigned char> downscaled;

                    rawproc->Get8bitThresholdedImage( weight_report, &downscaled, false );

                    int img_w = rawproc->Width();
                    int img_h = rawproc->Height();

                    delete rawproc;

                    // Apply datas to display boxes ...

                    boxLeftUp->label( _strStudyLU.c_str() );
                    boxRightUp->label( _strStudyRU.c_str() );
                    boxLeftDown->label( _strStudyLD.c_str() );
                    boxRightDown->label( _strStudyRD.c_str() );

                    unsigned char* downscaled_raw_buffer = NULL;
                    Fl_RGB_Image* newImg = NULL;
                    int imgSize = downscaled.size();

                    if ( imgSize > 0 )
                    {
                        downscaled_raw_buffer = new unsigned char[ imgSize * 3 ];

                        if ( downscaled_raw_buffer != NULL )
                        {
                            for( int cnt=0; cnt<imgSize; cnt++ )
                            {
                                unsigned char* refPixel = &downscaled_raw_buffer[cnt * 3];

                                refPixel[0] = downscaled[cnt];
                                refPixel[1] = downscaled[cnt];
                                refPixel[2] = downscaled[cnt];
                            }

                            newImg = new Fl_RGB_Image( downscaled_raw_buffer, img_w, img_h, 3 );

                            downscaled.clear();

                            if ( newImg != NULL )
                            {
                                comRView->unloadimage();
                                comRView->image( newImg );
                            }

                            if ( downscaled_raw_buffer != NULL )
                            {
                                delete[] downscaled_raw_buffer;
                                downscaled_raw_buffer = NULL;
                            }

                            delete newImg;

                            if ( boxStatus != NULL )
                            {
                                static char infostr[1024] = {0};
                                sprintf( infostr,
                                         "[%03d/%03d] %s",
                                         idx + 1,
                                         _dcmfiles.size(),
                                         _dcmfiles[idx].c_str() );
                                boxStatus->label( infostr );
                            }

                            comRView->position( idx + 1 );
                            comRView->redraw();
                            mainWindow->redraw();
                        }
                    }
                }
            }
        }

        CloseDCM();
    }
}

void wMain::WidgetCB( Fl_Widget* w )
{
    if ( w == mainWindow )
    {
        fl_message_title( "Program quit" );
        int retask = fl_ask( "Program may be terminated if you select YES, Proceed it ?" );

        if ( retask > 0 )
        {
            mainWindow->hide();
            delete mainWindow;
            mainWindow = NULL;
        }

        return;
    }
}

void wMain::MenuCB( Fl_Widget* w )
{
    Fl_Menu_* mw = (Fl_Menu_*)w;
    const Fl_Menu_Item* mi = mw->mvalue();
    if ( mi != NULL )
    {
        void* param = mi->user_data();
        if ( param != NULL )
        {
            if ( param == (void*)30 )
            {
                fl_message_title( "About this Program:" );
                fl_message( "rkDCMviewer,\n"
                            "\n"
                            "(C) Copyright 2016, 2017 Rageworx software, Raphael Kim (rageworx@@gmail.com)\n"
                            "*   This program is an open source project, and part of FLTK project.\n"
                            "*   All program codes by Rapahel Kay." );
            }
            else
            if ( param == (void*)31 )
            {
                fl_message_title( "Open sources:" );
                fl_message( "rkDCMviewer used these open source projects, \n"
                            "\n"
                            "- fltk-1.3.5-2-ts, https://github.com/rageworx/fltk-1.3.5-2-ts\n"
                            "- libtinydicom, https://github.com/rageworx/libtinydicom\n"
                            "- librawprocessor, https://github.com/rageworx/librawprocessor");
            }
        }
    }
}

void wMain::MoveCB( Fl_Widget* w )
{
    if ( w == mainWindow )
    {
        arrangeChildWindows();
    }
}

void wMain::OnRightClick( int x, int y)
{
}

void wMain::OnKeyPressed( unsigned short k, int s, int c, int a )
{
    if ( _keyprocessing == true )
        return;

    if ( comRView == NULL )
        return;

    _keyprocessing = true;

    int rcnidx = comRView->position();

    switch( k )
    {
        case FL_MOUSEWHEEL:
            {
                if ( c > 0 )
                {
                    if( rcnidx > 1 )
                        rcnidx--;
                }
                else
                if ( c < 0 )
                {
                    if ( rcnidx < comRView->maximum() )
                        rcnidx++;
                }
            }
            break;

        case FL_Home:
            rcnidx = 1;
            break;

        case FL_End:
            rcnidx = comRView->maximum();
            break;

        case FL_Left:
            if( rcnidx > 1 )
                rcnidx--;
            break;

        case FL_Right:
            if ( rcnidx < comRView->maximum() )
                rcnidx++;
            break;

        case FL_Escape:
            comRView->unloadimage();
            mainWindow->do_callback();
            _keyprocessing = false;
            return;

        default:
            _keyprocessing = false;
            return;
    }

    imageview( rcnidx - 1 );
}

void wMain::OnDrawCompleted()
{
    _keyprocessing = false;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void fl_w_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* wm = (wMain*)p;
        wm->WidgetCB( w );
    }
}

void fl_menu_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* wm = (wMain*)p;
        wm->MenuCB( w );
    }
}

void fl_move_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* wm = (wMain*)p;
        wm->MoveCB( w );
    }
}
